package com.android.nutrition.app.Utils

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.os.Environment
import android.util.DisplayMetrics
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.view.WindowManager
import cn.pedant.SweetAlert.SweetAlertDialog
import com.afollestad.materialdialogs.MaterialDialog
import com.android.nutrition.app.NutriApplication
import com.androidnetworking.error.ANError
import com.google.gson.ExclusionStrategy
import com.google.gson.FieldAttributes
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.realm.Realm
import io.realm.RealmObject
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

object Utils {
    fun copyDatabaseToSD() {
        val root = File(Environment.getExternalStorageDirectory(), "SCAN")
        if (!root.exists()) {
            root.mkdirs()
        }

        val file = "SCAN/default.realm"
        try {
            val sd = Environment.getExternalStorageDirectory()
            if (sd.canWrite()) {
                val currentDB = File(Realm.getDefaultInstance()!!.configuration.path)
                val backupDB = File(sd, file)
                if (currentDB.exists()) {
                    val src = FileInputStream(currentDB).channel
                    val dst = FileOutputStream(backupDB).channel
                    dst.transferFrom(src, 0, src.size())
                    src.close()
                    dst.close()
                }
            }
        } catch (e: Exception) {
            Log.e("copyDatabaseToSD", e.message)
        }
    }

    fun getMessageError(anError: ANError): String{
        if(!anError.message.isNullOrEmpty()){
            return anError.message.toString()
        }
        if(!anError.errorBody.isNullOrEmpty()){
            return anError.errorBody.toString()
        }
        return ""
    }

    fun getSweetAlert(context: Context, type: Int, title: String, message: String): SweetAlertDialog{
        val disabledBtnDialog = SweetAlertDialog(context, type)
            .setTitleText(title).setContentText(message)
        disabledBtnDialog!!.setOnShowListener { (disabledBtnDialog.getButton(SweetAlertDialog.BUTTON_CONFIRM).parent as View).visibility = View.GONE }
        disabledBtnDialog.setCancelable(false)
        return disabledBtnDialog
    }

    fun convertToBundle(map: Map<String, String>): Bundle {
        val bundle = Bundle()
        for (key in map.keys) {
            bundle.putString(Utils.getCapialLetter(key), map[key])
        }
        return bundle
    }

    fun getCapialLetter(letters: String): String {
        return letters.substring(0, 1).toUpperCase() + letters.substring(1)
    }

    fun isNetworkAvailable(): Boolean {
        val connectivityManager = NutriApplication.context!!.getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    fun convertJSONArrayToList(jsonArray: JSONArray): List<JSONObject>{
        val listdata = ArrayList<JSONObject>()
        for (i in 0 until jsonArray.length()) {
            listdata.add(jsonArray.getJSONObject(i))
        }
        return listdata
    }

    fun gson(): Gson {
        return GsonBuilder()
                .setExclusionStrategies(object : ExclusionStrategy {
                    override fun shouldSkipField(f: FieldAttributes): Boolean {
                        return f.declaringClass == RealmObject::class.java
                    }

                    override fun shouldSkipClass(clazz: Class<*>): Boolean {
                        return false
                    }
                })
                .create()
    }

    fun getNumColumns(windowManager: WindowManager, maxWidth: Int): Int{
        val width = getWidthScreen(windowManager)
        val res: Int = width/maxWidth
        return if (res == 0)  1 else res
    }

    fun getWidthScreen(windowManager: WindowManager): Int{
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)

        return displayMetrics.widthPixels
    }

    fun convertDpToPx(context: Context, dp: Int): Int {
        return Math.round(dp * (context.resources.displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))

    }

    fun convertPxToDp(px: Int): Int {
        return Math.round(px / (Resources.getSystem().getDisplayMetrics().xdpi / DisplayMetrics.DENSITY_DEFAULT))
    }

    fun distance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
        val theta = lon1 - lon2
        var dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta))
        dist = Math.acos(dist)
        dist = rad2deg(dist)
        dist = dist * 60.0 * 1.1515 //Millas
        dist = dist * 1.609344 //Kilometros
        dist = dist * 1000 //Metros
        //        if (unit == "K") {
        //            dist = dist * 1.609344;
        //        } else if (unit == "N") {
        //            dist = dist * 0.8684;
        //        }
        return dist
    }

    fun showMaterialDialog(activity: Activity, title: String, content: String, positiveText: String) {
        val materialDialogBuilder = MaterialDialog.Builder(activity)
                .title(title)
                .content(content)
                .positiveText(positiveText)

        val materialDialog = materialDialogBuilder.build()
        materialDialog.titleView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 19f)
        materialDialog.contentView!!.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 17f)
        materialDialog.show()
    }

    private fun deg2rad(deg: Double): Double {
        return deg * Math.PI / 180.0
    }

    private fun rad2deg(rad: Double): Double {
        return rad * 180 / Math.PI
    }
}