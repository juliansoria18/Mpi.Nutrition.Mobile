package com.android.nutrition.app.Config

import android.content.Context
import android.content.SharedPreferences
import com.android.nutrition.app.NutriApplication
import java.util.HashSet

/**
 * Created by USUARIO 004 on 27/8/2018.
 */
object SharedConfig {
    private val PREFS_FILENAME = "CONFIG_PREFS"
    private val TOKEN_CODE = "TOKEN_CODE"
    private val USER_NAME = "USER_NAME"
    private val USER_CODE = "USER_CODE"
    private val USER_SURNAME = "USER_SURNAME"
    private val USER_PHOTO = "USER_PHOTO"
    private val ORG_ID = "ORG_ID"
    private val ORG_COLOR = "ORG_COLOR"
    private val VERSION_DATA = "VERSION_DATA"
    private val SURVEY_ID = "SURVEY_ID"
    private val VERSION_ID = "VERSION_ID"
    private val FORM_ID = "FORM_ID"
    private val ORGBRANCH_ID = "ORGBRANCH_ID"
    private val LAST_LOCATION = "LAST_LOCATION"
    private val LAST_LOCATION_TMP = "LAST_LOCATION_TMP"
    private val START_CIRCUIT = "START_CIRCUIT"
    private val VERIFY_NUMBER = "VERIFY_NUMBER"
    private val DEVICE_ID = "DEVICE_ID"

    fun isStartCircuit(): Boolean {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)
        return settings.getBoolean(START_CIRCUIT, false)
    }

    fun setStartCircuit(lastLocations: Boolean) {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE) //1
        val editor: SharedPreferences.Editor = settings.edit() //2

        editor.putBoolean(START_CIRCUIT, lastLocations) //3

        editor.commit() //4
    }

    fun getLastLocationTmp(): Set<String> {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)
        return settings.getStringSet(LAST_LOCATION_TMP, HashSet<String>())
    }

    fun setLastLocationTmp(lastLocations: Set<String>) {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE) //1
        val editor: SharedPreferences.Editor = settings.edit() //2

        editor.putStringSet(LAST_LOCATION_TMP, lastLocations) //3

        editor.commit() //4
    }

    fun getLastLocation(): Set<String> {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)
        return settings.getStringSet(LAST_LOCATION, HashSet<String>())
    }

    fun setLastLocation(lastLocations: Set<String>) {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE) //1
        val editor: SharedPreferences.Editor = settings.edit() //2

        editor.putStringSet(LAST_LOCATION, lastLocations) //3

        editor.commit() //4
    }

    fun getOrganizationBranchId(): String {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)
        return settings.getString(ORGBRANCH_ID, "")
    }

    fun setOrganizationBranchId(organizationBranchId: String) {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE) //1
        val editor: SharedPreferences.Editor = settings.edit() //2

        editor.putString(ORGBRANCH_ID, organizationBranchId) //3

        editor.commit() //4
    }

    fun setFormnId(formId: String) {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE) //1
        val editor: SharedPreferences.Editor = settings.edit() //2

        editor.putString(FORM_ID, formId) //3

        editor.commit() //4
    }

    fun getFormnId(): String {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)
        return settings.getString(FORM_ID, "")
    }

    fun setVersionId(versionId: String) {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE) //1
        val editor: SharedPreferences.Editor = settings.edit() //2

        editor.putString(VERSION_ID, versionId) //3

        editor.commit() //4
    }

    fun getVersionId(): String {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)
        return settings.getString(VERSION_ID, "")
    }

    fun setSurveyId(surveyId: String) {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE) //1
        val editor: SharedPreferences.Editor = settings.edit() //2

        editor.putString(SURVEY_ID, surveyId) //3

        editor.commit() //4
    }

    fun getSurveyId(): String {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)
        return settings.getString(SURVEY_ID, "")
    }


    fun setVersionData(version: String) {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE) //1
        val editor: SharedPreferences.Editor = settings.edit() //2

        editor.putString(VERSION_DATA, version) //3

        editor.commit() //4
    }

    fun getVersionData(): String {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)
        return settings.getString(VERSION_DATA, "0")
    }

    fun setToken(token: String) {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE) //1
        val editor: SharedPreferences.Editor = settings.edit() //2

        editor.putString(TOKEN_CODE, token) //3

        editor.commit() //4
    }

    fun getToken(): String {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)
        return settings.getString(TOKEN_CODE, "")
    }

    fun setUserName(token: String) {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE) //1
        val editor: SharedPreferences.Editor = settings.edit() //2

        editor.putString(USER_NAME, token) //3

        editor.commit() //4
    }

    fun getUserName(): String {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)
        return settings.getString(USER_NAME, "")
    }

    fun setUserId(userId: String) {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE) //1
        val editor: SharedPreferences.Editor = settings.edit() //2

        editor.putString(USER_CODE, userId) //3

        editor.commit() //4
    }

    fun getUserId(): String {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)
        return settings.getString(USER_CODE, "")
    }

    fun setUserSurname(token: String) {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE) //1
        val editor: SharedPreferences.Editor = settings.edit() //2

        editor.putString(USER_SURNAME, token) //3

        editor.commit() //4
    }

    fun getUserSurname(): String {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)
        return settings.getString(USER_SURNAME, "")
    }

    fun setOrganizationColor(organizationColor: String) {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE) //1
        val editor: SharedPreferences.Editor = settings.edit() //2

        editor.putString(ORG_COLOR, organizationColor) //3

        editor.commit() //4
    }

    fun getOrganizationColor(): String {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)
        return settings.getString(ORG_COLOR, "")
    }

    fun setOrganizationId(organizationId: String) {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE) //1
        val editor: SharedPreferences.Editor = settings.edit() //2

        editor.putString(ORG_ID, organizationId) //3

        editor.commit() //4
    }

    fun getOrganizationId(): String {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)
        return settings.getString(ORG_ID, "")
    }

    fun setUserPhoto(token: String) {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE) //1
        val editor: SharedPreferences.Editor = settings.edit() //2

        editor.putString(USER_PHOTO, token) //3

        editor.commit() //4
    }

    fun getUserPhoto(): String {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)
        return settings.getString(USER_PHOTO, "")
    }

    fun clear(){
        SharedConfig.setToken("")
        SharedConfig.setUserId("")
        SharedConfig.setUserName("")
        SharedConfig.setUserPhoto("")
        SharedConfig.setUserSurname("")
        SharedConfig.setOrganizationId("")
        SharedConfig.setOrganizationColor("")
    }

    fun setVerifyNumber(tiene: Boolean) {
        val settings: SharedPreferences = NutriApplication.getAppContext()!!.getSharedPreferences(PREFS_FILENAME, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor
        editor = settings.edit()
        editor.putBoolean(VERIFY_NUMBER, tiene)
        editor.commit()
    }
}