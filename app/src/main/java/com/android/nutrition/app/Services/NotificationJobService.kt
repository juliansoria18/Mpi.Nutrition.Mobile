package com.android.nutrition.app.Services

/**
 * Created by USUARIO 004 on 16/3/2018.
 */

import android.util.Log
import com.android.nutrition.app.Config.SharedConfig
import com.android.nutrition.app.Utils.Task

import com.firebase.jobdispatcher.JobParameters
import com.firebase.jobdispatcher.JobService

class NotificationJobService : JobService() {

    override fun onStartJob(jobParameters: JobParameters): Boolean {
        val params2 = jobParameters.extras
        if (params2 != null && params2.containsKey("NotificationType")) {
            object: Task<String>(){
                override fun task(): String {
                    try {
                        if(!SharedConfig.getToken().isEmpty()){
//                            SyncService.syncronizate()
                        }
                    } catch (ex: Exception) {
                        Log.e("onStartJob", ex.message)
                    }
                    return ""
                }
            }.run()
        }
        return false
    }

    override fun onStopJob(jobParameters: JobParameters): Boolean {
        return false
    }

    companion object {

        private val TAG = "MyJobService"
    }

}