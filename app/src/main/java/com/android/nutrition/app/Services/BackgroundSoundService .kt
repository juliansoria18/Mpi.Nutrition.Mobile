package com.android.nutrition.app.Services

import android.app.Service
import android.media.MediaPlayer
import android.content.Intent
import android.os.IBinder
import android.util.Log
/**
 * Created by FSORIA 004 on 21/9/2018.
 */
class BackgroundSoundService {

    private val TAG = "BackgroundSoundService"
    var player: MediaPlayer? = null

    private object Holder { val INSTANCE = BackgroundSoundService() }
    companion object {
        val instance: BackgroundSoundService by lazy { Holder.INSTANCE }
    }

    fun onBind(arg0: Intent): IBinder? {
        Log.i(TAG, "onBind()")
        return null
    }

    fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        player!!.start()
        return Service.START_STICKY
    }


    fun onUnBind(arg0: Intent): IBinder? {
        Log.i(TAG, "onUnBind()")
        return null
    }

    fun onStop() {
        Log.i(TAG, "onStop()")
    }

    fun onPause() {
        Log.i(TAG, "onPause()")
    }

    fun onDestroy() {
        player!!.stop()
        player!!.release()
//        Toast.makeText(this, "Service stopped...", Toast.LENGTH_SHORT).show()
        Log.i(TAG, "onCreate() , service stopped...")
    }

    fun onLowMemory() {
        Log.i(TAG, "onLowMemory()")
    }
}