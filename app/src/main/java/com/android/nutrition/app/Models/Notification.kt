package com.android.nutrition.app.Models


/**
 * Created by USUARIO 004 on 23/3/2018.
 */

class Notification {
    var title: String = ""
    var text: String = ""
    var data: NotificationData? = null
    var destination: String = ""
}
