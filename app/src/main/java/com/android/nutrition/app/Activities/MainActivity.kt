package com.android.nutrition.app.Activities

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.android.nutrition.app.R
import android.content.Intent
import com.android.nutrition.app.Services.BackgroundSoundService


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        val bottomNavigationView = findViewById(R.id.navigation) as BottomNavigationView
//
//        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
//            var selectedFragment: Fragment? = null
//            when (item.itemId) {
//                R.id.action_item1 -> selectedFragment = ItemOneFragment.newInstance()
//                R.id.action_item2 -> selectedFragment = ItemTwoFragment.newInstance()
//                R.id.action_item3 -> selectedFragment = ItemThreeFragment.newInstance()
//            }
//            val transaction = supportFragmentManager.beginTransaction()
//            transaction.replace(R.id.frame_layout, selectedFragment!!)
//            transaction.commit()
//            true
//        }

        //Manually displaying the first fragment - one time only

//        val transaction = supportFragmentManager.beginTransaction()
//        transaction.replace(R.id.frame_layout, ItemOneFragment.newInstance())
//        transaction.commit()

        //Used to select an item programmatically
        //bottomNavigationView.getMenu().getItem(2).setChecked(true);
    }
}

