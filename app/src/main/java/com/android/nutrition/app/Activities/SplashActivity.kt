package com.android.nutrition.app.Activities

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.WindowManager
import com.android.nutrition.app.Config.SharedConfig
import com.android.nutrition.app.R
import com.android.nutrition.app.Utils.Task
import com.android.nutrition.app.Utils.Utils

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)

        object: Task<String>(){
            override fun task(): String{
                try {
                    Thread.sleep(1700)
                    goNextPage()
                }catch (e: Exception){
                    Log.e("SplashActivity", e.message)
                }
                return ""
            }
        }.run()
    }

    private fun validLogin(){
        if(SharedConfig.getToken().isEmpty()){
            val i = Intent(applicationContext, PhoneActivity::class.java)
            startActivity(i)
        }else{
            val i = Intent(applicationContext, PermissionRequestActivity::class.java)
            startActivity(i)
        }
        finish()
    }


    private fun goNextPage(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(this.checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                    this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    this.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){

                if(SharedConfig.getToken().isEmpty()){
                    val i = Intent(applicationContext, PhoneActivity::class.java)
                    startActivity(i)
                    finish()
                    return
                }
                val i = Intent(this, MainActivity::class.java)
                startActivity(i)
                finish()
                if (Utils.isNetworkAvailable()) {
                    object : Thread() {
                        override fun run() {
//                            SyncService.syncronizate()
                        }
                    }.run()
                }
                return
            }
        }
        validLogin()
    }

}
