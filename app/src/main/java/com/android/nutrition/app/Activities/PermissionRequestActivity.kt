package com.android.nutrition.app.Activities

import android.Manifest
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.android.nutrition.app.R
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import kotlinx.android.synthetic.main.activity_permission.*
import org.jetbrains.anko.indeterminateProgressDialog

class PermissionRequestActivity : AppCompatActivity() {

    private var dialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_permission)

        request_camera.setOnClickListener {
            val permissions = arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
            )
            Permissions.check(this, permissions, null, null, object : PermissionHandler() {
                override fun onGranted() {
                    downloadCatalogs()
                }
            })
        }
    }

    private fun downloadCatalogs(){
        dialog = indeterminateProgressDialog(message = "Descargando datos, por favor espere un momento...")
        dialog!!.setCancelable(false)
        dialog!!.show()
//        val listDown = ArrayList<RepositoryInterface<*>>()
//        listDown.add(CorridorRepository.instance)
//        listDown.add(BusRepository.instance)
//        listDown.add(BeneficiaryTypeRepository.instance)
//
//        val contadorService = object : TaskIterator<RepositoryInterface<*>>(listDown) {
//            override fun actionEnd() {
                dialog!!.dismiss()
                goNextPage()
//            }
//
//            override fun actionItem(item: RepositoryInterface<*>) {
//                item.download()
//            }
//        }
//        contadorService.simultaneousWork = 3
//        contadorService.eventNext = "download_finish"
//        contadorService.start()
    }

    private fun goNextPage(){
        val i = Intent(this, MainActivity::class.java)
        startActivity(i)
        finish()
    }
}
