package com.android.nutrition.app.Utils

import android.util.Log
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by USUARIO 004 on 5/9/2018.
 */
object DateUtils {
    @JvmStatic
    fun toSimpleString(date: Date) : String {
        var result = ""
        try {
            val format = SimpleDateFormat("dd/MM/yyy")
            result = format.format(date)
        }catch (e: Exception){
            Log.e("toCompleteString", e.message)
        }
        return result
    }

    @JvmStatic
    fun toCompleteString(date: Date) : String {
        var result = ""
        try {
            val format = SimpleDateFormat("dd/MM/yyy HH:mm")
            result = format.format(date)
        }catch (e: Exception){
            Log.e("toCompleteString", e.message)
        }
        return result
    }

    @JvmStatic
    fun toSimpleString(date: String) : String {
        var result = ""
        try {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
            val dateNew = simpleDateFormat.parse(date)
            val format = SimpleDateFormat("dd/MM/yyy")
            result = format.format(dateNew)
        }catch (e: Exception){
            Log.e("toCompleteString", e.message)
        }
        return result
    }

    @JvmStatic
    fun converSimpleToBaseFormat(date: String) : String {
        var result = ""
        try {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            val format = SimpleDateFormat("dd/MM/yyy")
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
            val dateNew = format.parse(date)
            result = simpleDateFormat.format(dateNew)
        }catch (e: Exception){
            Log.e("toCompleteString", e.message)
        }
        return result
    }

    @JvmStatic
    fun toCompleteString(date: String) : String {
        var result = ""
        try {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
            val dateNew = simpleDateFormat.parse(date)
            val format = SimpleDateFormat("dd/MM/yyy HH:mm")
            result = format.format(dateNew)
        }catch (e: Exception){
            Log.e("toCompleteString", e.message)
        }
        return result
    }

    @JvmStatic
    fun toCompleteInverseString(date: String) : String {
        var result = ""
        try {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
            val dateNew = simpleDateFormat.parse(date)
            val format = SimpleDateFormat("HH:mm dd/MM/yyy")
            result = format.format(dateNew)
        }catch (e: Exception){
            Log.e("toCompleteString", e.message)
        }
        return result
    }

    @JvmStatic
    fun convertDateToString(date: Date): String {
        var result = ""
        try {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
            result = simpleDateFormat.format(date)
        }catch (e: Exception){
            Log.e("convertDateToString", e.message)
        }
        return result
    }

    @JvmStatic
    fun convertDateToStringInverse(date: String) : Date? {
        var result: Date? = null
        try {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
            result = simpleDateFormat.parse(date)
        }catch (e: Exception){
            Log.e("convertDateToStringInv", e.message)
        }
        return result
    }

    @JvmStatic
    fun convertSecondToString(sec: Int): String {
        var result = ""
        try {
            var minutes: Int = sec / 60
            var second = sec - (minutes * 60)
            var minutesString = if(minutes == 0) "" else "$minutes min "
            var secondString = "$second sec"
            return "$minutesString$secondString"
        }catch (e: Exception){
            Log.e("convertDateToString", e.message)
        }
        return result
    }

    @JvmStatic
    fun convertToShortFormatSlash(date: String) : String {
        var result = ""
        try {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
            val dateNew = simpleDateFormat.parse(date)
            val format = SimpleDateFormat("dd/MM/yyyy")
            result = format.format(dateNew)
        }catch (e: Exception){
            Log.e("toCompShortFormatSlash", e.message)
        }
        return result
    }

    @JvmStatic
    fun convertToShortFormatBase(date: String) : String {
        var result = ""
        try {
            val simpleDateFormat = SimpleDateFormat("dd/MM/yyyy")
            val dateNew = simpleDateFormat.parse(date)
            val format = SimpleDateFormat("yyyy-MM-dd")
            result = format.format(dateNew)
        }catch (e: Exception){
            Log.e("convertToShortFormatB", e.message)
        }
        return result
    }

    @JvmStatic
    fun toShortFormatSlash(date: String) : Date? {
        var result: Date? = null
        try {
            val simpleDateFormat = SimpleDateFormat("dd/MM/yyyy")
            result = simpleDateFormat.parse(date)
        }catch (e: Exception){
            Log.e("toCompShortFormatSlash", e.message)
        }
        return result
    }

    @JvmStatic
    fun convertToUniversalCode(date: String) : String {
        var result = ""
        try {
            val simpleDateFormat = SimpleDateFormat("dd/MM/yyy")
            val dateNew = simpleDateFormat.parse(date)
            val format = SimpleDateFormat("yyMMdd")
            result = format.format(dateNew)
        }catch (e: Exception){
            Log.e("convertToUniversalCode", e.message)
        }
        return result
    }

    @JvmStatic
    fun convertTraditionalDate(date: String) : Date? {
        var result: Date? = null
        try {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            result = simpleDateFormat.parse(date)
        }catch (e: Exception){
            Log.e("convertToUniversalCode", e.message)
        }
        return result
    }

    @JvmStatic
    fun getCalentarCustom(day: Int? = null, month: Int? = null, year: Int? = null, hour: Int? = null, minute: Int? = null) : Calendar {
        val calendar = Calendar.getInstance()
        try {
            calendar.time = Date()
            if(day != null) calendar.set(Calendar.DAY_OF_MONTH, day)
            if(month != null) calendar.set(Calendar.MONTH, month - 1)
            if(year != null) calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.HOUR_OF_DAY, if(hour != null) hour else 0)
            calendar.set(Calendar.MINUTE, if(minute != null) minute else 0)
            calendar.set(Calendar.SECOND, 0)
        }catch (e: Exception){
            Log.e("convertToUniversalCode", e.message)
        }
        return calendar
    }

    @JvmStatic
    fun getDayIdByDayName(dateName: String) : String {
        return when(dateName){
            "MON" -> "COD_LUNES"
            "TUE" -> "COD_MARTES"
            "WED" -> "COD_MIERCOLES"
            "THU" -> "COD_JUEVES"
            "FRI" -> "COD_VIERNES"
            "SAT" -> "COD_SABADO"
            "SUN" -> "COD_DOMINGO"
            else -> ""
        }
    }
}